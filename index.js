const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

const firestore = functions.firestore;

exports.onUserStatusChange = functions.database
    .ref('/status/{userId}')
    .onUpdate((event, context) => {
        var db = admin.firestore();
        var fieldValue = require("firebase-admin").firestore.FieldValue;
        
        //const usersRef = firestore.document('/users/' + event.params.userId);
        const usersRef = db.collection("users");
        var snapShot = event.after;
        
        console.log('UPDATE STATUS', snapShot);
        console.log('UPDATE STATUS', event);
        
        return event.after.ref.once('value')
            .then(statusSnap => snapShot.val())
            .then(status => {
                if (status === 'offline') {
                    console.log("User is online, changing to offline now");
                    usersRef
                        .doc(context.params.userId)
                        .set({
                            chat_room: "",
                            matched: false,
                            online: false,
                            // last_active: fieldValue.serverTimestamp()//Date.now()
                        }, { merge: true });
                }
            })
    });